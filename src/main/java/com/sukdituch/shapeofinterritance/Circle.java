/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeofinterritance;

/**
 *
 * @author focus
 */
public class Circle extends Shape {

    public Circle(double s, double w, double l, double h, double b, double r) {
        super(s, w, l, h, b, r);//default constructor
        calArea = 0.5 * (r * r) * 3.14;
        System.out.println("created: circle");
    }

    @Override
    public void CircleArea() {
        //super.CircalArea();
        System.out.println("Circle: " + "radius: " + this.r + " area => " + (this.calArea));
    }

}
