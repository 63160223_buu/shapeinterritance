/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeofinterritance;

/**
 *
 * @author focus
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(0, 0, 0, 0, 0, 3);
        circle1.CircleArea();
        System.out.println(" ");

        Circle circle2 = new Circle(0, 0, 0, 0, 0, 4);
        circle2.CircleArea();
        System.out.println(" ");

        Triangle triangle = new Triangle(0, 0, 0, 3, 4, 0);
        triangle.TriangleArea();
        System.out.println(" ");

        Rectangle rectangle = new Rectangle(0, 4, 0, 4, 0, 0);
        rectangle.RectangleArea();
        System.out.println(" ");
        

    }
}
