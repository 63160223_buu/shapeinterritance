/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeofinterritance;

/**
 *
 * @author focus
 */
public class Triangle extends Shape {

    public Triangle(double s, double w, double l, double h, double b, double r) {
        super(s, w, l, h, b, r);//default constructor
        calArea = 0.5 * h * b;
        System.out.println("created: Triangle");
    }

    @Override
    public void TriangleArea() {
        System.out.println("Triangle: " + "high: " + this.h + " base: " + this.b
                + " area => " + (this.calArea));
    }
}
